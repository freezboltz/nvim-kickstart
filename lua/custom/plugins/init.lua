-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- See the kickstart.nvim README for more information
return {
  {
    'b0o/schemastore.nvim',
    lazy = true,
    opts = {},
  },
  {
    'ThePrimeagen/vim-be-good',
    enabled = true,
  },
  {
    'rebelot/kanagawa.nvim',
    enabled = false,
    lazy = false,
    priority = 1000,
    opts = {},
    config = function()
      vim.cmd.colorscheme 'kanagawa'
    end,
  },
  {
    'LunarVim/darkplus.nvim',
    enabled = false,
    lazy = false,
    priority = 1000,
    opts = {},
    config = function()
      vim.cmd.colorscheme 'darkplus'
    end,
  },
  {
    'tiagovla/tokyodark.nvim',
    enabled = false,
    opts = {},
    config = function(_, opts)
      require('tokyodark').setup(opts) -- calling setup is optional
      vim.cmd.colorscheme 'tokyodark'
    end,
  },
  {
    'ellisonleao/gruvbox.nvim',
    priority = 1000,
    enabled = true,
    opts = {},
    config = function(_, opts)
      vim.cmd.colorscheme 'gruvbox'
    end,
  },
  {
    'gmr458/vscode_modern_theme.nvim',
    enabled = false,
    lazy = false,
    priority = 1000,
    config = function()
      require('vscode_modern').setup {
        cursorline = true,
        transparent_background = false,
        nvim_tree_darker = true,
      }
      vim.cmd.colorscheme 'vscode_modern'
    end,
  },
}
